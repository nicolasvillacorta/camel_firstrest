1) Agregue dependencias camel-servlet-starter y camel jackson.
2) Cambie la version del Camel, del carmel servet y del camel jackson de "2.24.0" a "3.0.0-M4".
3) El proyecto lleva Lombok, ademas de agregar la dependencia para el proyecto, tuve que descargar el jar e
	instalarlo en el IDE para que el mismo lo reconozca. (Se puede verificar que este instalado abajo de todo
	en la ventana: Help->About [Caso Eclipse]). Hay que hacer Maven->Update Project luego para que funcio
	
NOTA IMPORTANTE:
	Si uso el camel y sus 2 dependencias (servlet-starter y jackson) en las versiones que trae STS (2.24.0)
		tengo que usar en el RouterBuilder la anotacion '@BeanInject' para inyectar el processor. 
	Si uso el camel y sus 2 dependencias con las versiones mas nuevas (3.0.0-M4), puedo usar tranquilamente
		el '@Autowired'
