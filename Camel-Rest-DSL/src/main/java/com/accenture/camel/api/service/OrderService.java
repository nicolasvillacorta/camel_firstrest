package com.accenture.camel.api.service;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;

import org.springframework.stereotype.Service;

import com.accenture.camel.api.dto.Order;

@Service
public class OrderService {
// La idea es fakear una DB.
	private List<Order> list = new ArrayList<>();

	// Esta anotacion se usa en un metodo que necesita ejecutarse despues que de se inyecte la dependencia, para
	// evitar problemas de inicializacion. Si no esta la anotacion, al llamar en un endpoint a getOrders(), devuelve
	// una lista vacia.
	@PostConstruct
	public void initDB() {
		list.add(new Order(67, "Mobile", 500));	
		list.add(new Order(89, "Book", 400));	
		list.add(new Order(45, "AC", 15000));	
		list.add(new Order(22, "Shoes", 4000));	
	}
	
	public Order addOrder(Order order) {
		list.add(order);
		return order;
	}
	
	public List<Order> getOrders(){
		return list;
	}
	
}
