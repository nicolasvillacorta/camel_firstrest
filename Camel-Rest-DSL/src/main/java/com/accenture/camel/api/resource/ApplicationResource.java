package com.accenture.camel.api.resource;

import org.apache.camel.BeanInject;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.model.rest.RestBindingMode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;

import com.accenture.camel.api.dto.Order;
import com.accenture.camel.api.processor.OrderProcessor;
import com.accenture.camel.api.service.OrderService;

// Un resource es similar a un controlador

@Component
public class ApplicationResource extends RouteBuilder {

	@Autowired
	private OrderService service;

	@Autowired
	private OrderProcessor processor;
	
	@Override
	public void configure() throws Exception {
		// En esta linea elijo usar el camel servlet, seteo puerto que va a usar el servlet (lo aclare tambien 
		// en el application.properties) y que todas las request intercambien json.
		restConfiguration().component("servlet").port(9090).bindingMode(RestBindingMode.json);
		
		// Endpoint rest --> Todo esto es lo mismo que un endpoint en un metodo en jax-rs o springweb.
		// Los produces() no hacen falta ya que tengo el bindingMode.
		rest().get("/hello").produces(MediaType.APPLICATION_JSON_VALUE)
			.route().setBody(constant("Welcome to Java Techie")).endRest();
		
		rest().get("/getOrders").produces(MediaType.APPLICATION_JSON_VALUE)
			.route().setBody(()->service.getOrders()).endRest();
		
		// El POST necesita para funcionar un processor (procesador)
		rest().post("/addOrder").consumes(MediaType.APPLICATION_JSON_VALUE)
			.type(Order.class).outType(Order.class).route().process(processor).endRest();
	}	

}
