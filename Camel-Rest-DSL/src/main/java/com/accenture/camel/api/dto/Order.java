package com.accenture.camel.api.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data // Genera getter, setter y el ToString (Igual que las otras 3 anotaciones de Lombok juntas)
@AllArgsConstructor
@NoArgsConstructor
public class Order {

	private int id;
	private String name;
	private double price;
	
}
