package com.accenture.camel.api.processor;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.accenture.camel.api.dto.Order;
import com.accenture.camel.api.service.OrderService;

// El processor debe implementar Processor de camel y debe ser un componente, que con Autowired se inyectara
// en el RouteBuilder ( en este caso ApplicationResource.class)
@Component
public class OrderProcessor implements Processor{

	@Autowired
	private OrderService service;
	
	@Override
	public void process(Exchange exchange) throws Exception {
		service.addOrder(exchange.getIn().getBody(Order.class));
	}

}
